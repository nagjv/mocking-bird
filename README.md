# mocking-bird
An extendable and configurable mocking service

## System Requirements:

- NODEJS >v12.13.0
- NPM >6.4.1
- Yarn >1.13.0

## Setup:

- Clone this repo
- Run `yarn install`
- Run `yarn build`
- Run `yarn start` or `yarn watch-debug` for local development

## Docker
- Build `docker build -t mocking-bird .`
- Run `docker run -p 8008:8008 mocking-bird`
