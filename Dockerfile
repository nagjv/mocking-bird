FROM node:15.3-alpine3.10 As development

WORKDIR /usr/src/app

COPY package*.json ./

RUN yarn install

COPY . .

RUN yarn build


FROM node:15.3-alpine3.10 as production

ARG PORT=8008
ENV PORT=${PORT}

WORKDIR /usr/src/app

COPY package*.json ./

RUN yarn install --prod

COPY . .

COPY --from=development /usr/src/app/dist ./dist

EXPOSE ${PORT}

CMD ["yarn", "start"]