import request from 'supertest';
import app from '../../app';

describe('GET /ping', () => {
  it('should return 200 pong', (done) => {
    request(app).get('/ping')
      .expect(200, done);
  });
});
