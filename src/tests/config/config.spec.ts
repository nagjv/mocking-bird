const jsonSchema = require('../../config/mock-schema.json');
const jsonData = require('../../config/mock.json');
const validate = require('jsonschema').validate;

describe('Validate Mock JSON', () => {
  it('matches the schema', async () => {
    const res = validate(jsonData, jsonSchema);
    console.log('#####:::', res.valid);
    expect(res.valid).toBeTruthy();
  });
});