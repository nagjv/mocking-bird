import errorHandler from 'errorhandler';
import app from './app';

app.use(errorHandler());

const server = app.listen(app.get('port'), () => {
  console.log(
    'App is running at http://localhost:%d',
    app.get('port')
  );
  console.log('  Press CTRL-C to stop\n');
});

export default server;
