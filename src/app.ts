import express from 'express';
import router from './routes/routes';
require('dotenv').config();

const app = express();

const PORT = process.env.PORT || 3003;

app.set('port', PORT);
app.use(express.json());
app.use(router);

export default app;
