import {Router} from 'express';
import {ping, doSomething} from '../controllers/controller';

const router:Router = Router();

router.get('/ping', ping);
router.get('/mock/*', doSomething);
router.post('/mock/*', doSomething);
router.put('/mock/*', doSomething);
router.delete('/mock/*', doSomething);

export default router;
