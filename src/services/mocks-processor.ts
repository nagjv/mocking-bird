const jsonData = require('../config/mock.json');

export default class MocksProcessor {

    req_count: Record<string, number> = {}; //count of requests

    constructor() { }

    public processor = (reqURL: string, reqMethod: string) => {

        if(jsonData[reqURL] && jsonData[reqURL][reqMethod]) {
            const fail_percent = jsonData[reqURL][reqMethod].fail_percent || 0;
            this.req_count[`${reqMethod}:${reqURL}`] = this.req_count[`${reqMethod}:${reqURL}`]? this.req_count[`${reqMethod}:${reqURL}`]+1 : 1;
            
            console.log(`${reqMethod}:${reqURL} ::`, this.req_count[`${reqMethod}:${reqURL}`]);

            if(fail_percent === 'random') {
                const random_number = Math.floor(Math.random() * 100);
                if(random_number % 5 == 0 || random_number % 9 == 0)
                    return jsonData[reqURL][reqMethod].response.fail;
            }
            else if(fail_percent == 50 && this.req_count[`${reqMethod}:${reqURL}`]%2 == 0)
                return jsonData[reqURL][reqMethod].response.fail;

            return jsonData[reqURL][reqMethod].response.success;
        }

        return {'message': {'status':'Invalid mock config', 'mock_url': `${reqURL}`, 'method':`${reqMethod}`},'code': 404};
    }
}
