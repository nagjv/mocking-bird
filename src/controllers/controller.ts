import { Response, Request } from 'express';
import MocksProcessor from '../services/mocks-processor';

const mockProcessor = new MocksProcessor();

export const ping = async (_: Request, res: Response) => {
  res.status(200).json({'message':'pong'});
 };

export const doSomething = async(req: Request, res: Response): Promise<void> => {
  console.log(buildLog(req));
  const resp_obj = mockProcessor.processor(req.url, req.method);
  res.status(resp_obj.code).json(resp_obj.message);
};

const buildLog = async(req: Request) => {
  const log_obj = {
    req_url: req.url,
    method: req.method,
    headers: req.headers,
    webhook_payload:req.body
  };
  return log_obj;
};